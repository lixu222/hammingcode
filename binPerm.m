function A = binPerm(n)
% calculate all binary permutations for n bits.
% output is n-by-2^n matrix with all 0 column at first column & all 1 column at last column
% each column represent decimal number in binary in ascending order
% first row represent LSB & last row represent MSB
% n must > 0

A = zeros(n, 2^n);
for i = 1:n
   r = 1:2^n;
   r = r-1;
   r = floor(r/2^(i-1));
   r = rem(r, 2);
   A(i, :) = r;
end
