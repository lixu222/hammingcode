function displayV(X, verbosity)
% wrapper for display, which only print out X if verbosity = true

if verbosity
   disp([inputname(1) ' =']);
   disp(' ');
   disp(X);
end
