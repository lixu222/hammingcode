function code=genHamming74(data)
% generate Hamming code from 4-bit data to 7-bit code.
% data can also be 4xN matrix with each column representing 4-bit data
% IMPORTANT: data(1,:), first row from top down, represent LSB. data(N,:), last from top down, represent MSB

G=[
    1,0,0,0; % b0
    0,1,0,0; % b1
    0,0,1,0; % b2
    0,0,0,1; % b3
    0,1,1,1; % p2
    1,0,1,1; % p1
    1,1,0,1; % p0
];
code=rem(G*data,2);
