function sig = chkHam84(code)
% generate check signature from hamming (8,4) code
% if signature is non-zero & code has 1-bit error, then LSB = 1 to indicate
% single-bit error, sig[MSB:1] indicate error bit index (0-based)
% for code with 2-bit error, LSB = 0, but error cannot be corrected
% for code with > 2-bit error, signature may not be zero, and could instead
% indicate 1-bit or 2-bit error
% IMPORTANT: code is vertical vector with LSB=first row and MSB=last row
H =[
    1, 1, 1, 1, 1, 1, 1, 1;
    0, 1, 0, 1, 0, 1, 0, 1;
    0, 0, 1, 1, 0, 0, 1, 1;
    0, 0, 0, 0, 1, 1, 1, 1;
];
sig = rem(H * code, 2);
