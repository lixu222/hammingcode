function code=genHam84(data)
% generate Hamming code from 4-bit data to 8-bit code. correct 1-bit error
% & detect 2-bit error
% data can also be 4xN matrix with each column representing 4-bit data
% IMPORTANT: data(1,:), first row from top down, represent LSB. data(N,:), last from top down, represent MSB

G=[
    1,0,0,0; % b0
    0,1,0,0; % b1
    0,0,1,0; % b2
    0,0,0,1; % b3
    0,1,1,1; % p2
    1,0,1,1; % p1
    1,1,0,1; % p0
];
code = rem(G*data,2);

% generate overall parity for all code
% overall parity is code's LSB
J = [ones(1, 7); eye(7);];
code = rem(J*code, 2);
