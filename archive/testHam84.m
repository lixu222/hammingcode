function err = testHam84
% test Hamming code generation and decoding for 4-bit data & 8-bit code

% generate all possible 4-bit data in binary with each data stored in a
% column
M = 4; % data width
N = 2^4;
Mc = 8; % code width
Ms = 4; % signature width
err = false;
data = binPerm(M);
display(data);

code = genHam84(data);
display(code);

sig = chkHam84(code);
if sum(reshape(sig, [], 1)) ~= 0
   display('ERROR: H*code for correct code do not equal 0');
   err = true;
   return;
end

errIndex = Mc - randi(Mc, 1, N); % index 0-based
display(errIndex);

% add 1 to each column at one random row to zero matrix
code1bErr = zeros(Mc, N);
for j = 1:N
    code1bErr(errIndex(j) + 1, j) = 1;
end
display('add random 1 bit error to otherwise correct code');
code1bErr = rem(code + code1bErr, 2);
display(code1bErr);

% calculate signature for code with 1 bit error
sig1bErr = chkHam84(code1bErr);
display(sig1bErr);

base2 = 2.^([0:Ms-2]);
sig1bErrIndex = base2 * sig1bErr(2:end, :);
display(sig1bErrIndex);

if sum(sig1bErrIndex - errIndex) == 0
    display('PASS: actual & expected 1-bit error position match');
else
    display('FAIL: actual & expected 1-bit error position do not match');
    err = true;
    return;
end

% generate code with 2-bit error
code2bErr = zeros(Mc, N);
for j = 1:N
    % 2-bit error position must be diff
    err1Index = randi(Mc, 1);
    err2Index = randi(Mc, 1);
    while err2Index == err1Index
        err2Index = randi(Mc, 1);
    end
    code2bErr(err1Index, j) = 1;
    code2bErr(err2Index, j) = 1;
end
code2bErr = rem(code2bErr + code, 2);
display(code2bErr);

% calculate check result for code with 2-bit error
sig2bErr = chkHam84(code2bErr);
display(sig2bErr);

if sum(sig2bErr(1, :)) ~= 0
    display('FAIL: did not detect 2-bit error');
    err = true;
    return;
end

% generate code with 3-bit error
code3bErr = zeros(Mc, N);
for j = 1:N
    % 3-bit error positon must be diff
    err1Index = randi(Mc, 1);
    err2Index = randi(Mc, 1);
    while err2Index == err1Index
        err2Index = randi(Mc, 1);
    end
    err3Index = randi(Mc, 1);
    while err3Index == err2Index || err3Index == err1Index
        err3Index = randi(Mc, 1);
    end
    code3bErr(err1Index, j) = 1;
    code3bErr(err2Index, j) = 1;
    code3bErr(err3Index, j) = 1;
end
code3bErr = rem(code3bErr + code, 2);
display(code3bErr);

% calculate check result for code with 3-bit error
sig3bErr = chkHam84(code3bErr);
display(sig3bErr);
