function sig = chkHamming74(code)
% generate check signature from hamming (7,4) code
% if signature is non-zero & code has 1-bit error, signature indicate error bit index in code
% chk also work for data width < 4, such as (7,3), (7,2)
H =[
    1, 0, 1, 0, 1, 0, 1;
    0, 1, 1, 0, 0, 1, 1;
    0, 0, 0, 1, 1, 1, 1;
];
sig = rem(H * code, 2);
