function err = testHamAll(codeWidth, dataWidth, verb)
% exhaustively test (codeWidth, dataWidth) Hamming code generation and decoding for all data
% verb show extra info if true
% all possible code for given (codeWidth, dataWidth) is generated
% one of each: 1-, 2-, 3-bit error, are injected to each code
% for 1-bit error, overall parity is checked to be 1,
% and individual parity match error bit index (0-based)
% for 2-bit error, overall parity is checked to be 0
% for 3-bit error, no check is done 

N = 2^dataWidth;
Ms = codeWidth - dataWidth; % signature width
Mc = 2^(codeWidth - dataWidth - 1); % true code width with data 0-filled
err = false;
data = binPerm(dataWidth);
displayV(data, verb);

[code, codeStd, err] = genHam(codeWidth, dataWidth, data);
if err == true
   display('ERROR: genHam');
   return;
end
displayV(code, verb); 
displayV(codeStd, verb);

sig = chkHam(codeWidth, dataWidth, code);
if sum(reshape(sig, [], 1)) ~= 0
   display('ERROR: H*code for correct code do not equal 0');
   err = true;
   return;
end

errIndex = Mc - randi(Mc, 1, N); % index 0-based
displayV(errIndex, verb);

% add 1 to each column at one random row to zero matrix
code1bErr = zeros(Mc, N);
for j = 1:N
    code1bErr(errIndex(j) + 1, j) = 1;
end
displayV('add random 1 bit error to otherwise correct code', verb);
code1bErr = rem(code + code1bErr, 2);
displayV(code1bErr, verb);

% calculate signature for code with 1 bit error
sig1bErr = chkHam(codeWidth, dataWidth, code1bErr);
displayV(sig1bErr, verb);

base2 = 2.^([0:Ms-2]);
sig1bErrIndex = base2 * sig1bErr(2:end, :);
displayV(sig1bErrIndex, verb);

if sum(sig1bErr(1,:), 2) ~= size(data, 2)
   display('Error: overall parity is not 1');
   err = true;
   return;
end
if sum(sig1bErrIndex - errIndex) == 0
    displayV('PASS: actual & expected 1-bit error position match', verb);
else
    display('Error: actual & expected 1-bit error position do not match');
    err = true;
    return;
end

% generate code with 2-bit error
code2bErr = zeros(Mc, N);
for j = 1:N
    % 2-bit error position must be diff
    err1Index = randi(Mc, 1);
    err2Index = randi(Mc, 1);
    while err2Index == err1Index
        err2Index = randi(Mc, 1);
    end
    code2bErr(err1Index, j) = 1;
    code2bErr(err2Index, j) = 1;
end
code2bErr = rem(code2bErr + code, 2);
displayV(code2bErr, verb);

% calculate check result for code with 2-bit error
sig2bErr = chkHam(codeWidth, dataWidth, code2bErr);
displayV(sig2bErr, verb);

if sum(sig2bErr(1, :), 2) ~= 0
    display('Error: did not detect 2-bit error');
    err = true;
    return;
end

% generate code with 3-bit error
code3bErr = zeros(Mc, N);
for j = 1:N
    % 3-bit error positon must be diff
    err1Index = randi(Mc, 1);
    err2Index = randi(Mc, 1);
    while err2Index == err1Index
        err2Index = randi(Mc, 1);
    end
    err3Index = randi(Mc, 1);
    while err3Index == err2Index || err3Index == err1Index
        err3Index = randi(Mc, 1);
    end
    code3bErr(err1Index, j) = 1;
    code3bErr(err2Index, j) = 1;
    code3bErr(err3Index, j) = 1;
end
code3bErr = rem(code3bErr + code, 2);
displayV(code3bErr, verb);

% calculate check result for code with 3-bit error
sig3bErr = chkHam(codeWidth, dataWidth, code3bErr);
displayV(sig3bErr, verb);
