function [code, codeStd, err] = genHam(codeWidth, dataWidth, data)
% generate 1-bit error correct, 2-bit error detect hamming code from input data for (codeWidth, dataWidth) hamming.
% number of parity bit including overall parity = codeWidth - dataWidth
% data LSB is first row & MSB is last row
%
% code has following form
% [pA; data[LSB]; data[LSB+1]; ... data[MSB]; p[MSB]; ... p[LSB];]
% where pA is overall parity bit; p[MSB:LSB] are individual parity bit;
% IMPORTANT: number of code rows = 2^(number of parity bit excluding overall parity) 
% eg, for (7,3) hamming code, there are 3 parity bit excluding overall parity.
% data can be 4 bit cuz 2^3 = 8 bit (1 overall parity, 3 individual parity, 4 data)
% because input data is only 3 bit, code row # still equal 8 bit, and 4th data bit is auto zeroed
%
% codeStd (standard form) has following form
% first row is overall parity bit
% individual parity bit are at 2^parity bit positions.
% eg, for (8,4) hamming code, exclude overall parity, there are 3 parity bit
% parity bit 0 is at 2^0=1, or position 1 (LSB), index-1 based
% parity bit 1 is at 2^1=2, or position 2
% parity bit 2 is at 2^2=4, or position 4
% with 4 data bit, standard form = [pA; p0; p1; b0; p2; b1; b2; b3;]
% generated standard form vertical vector do not calculate parity, simply filling input parity at correct bit position
% data is used to fill data bit positions from LSB.
% if there are more data bit to fill than data available, data bit is 0-filled
% standard form vector size always = 2^Np - 1; 

  
err = false; 
Np = codeWidth - 1 - dataWidth; % # of individual parity bit excluding overall parity
if 2^Np < codeWidth
   display('ERROR: not enough code bit for given data');
   err = true;
   return;
end
code = zeros(2^Np, size(data, 2));

codeStd = stdForm(Np, data, zeros(Np,1));

% parity generating matrix, excluding overall parity
G = binPerm(Np);
G = G(:, 2:end); % remove 1st column 

par = rem(G*codeStd, 2); % LSB is first row
code(2:size(data,1)+1, :) = data;
code(end-size(par,1)+1:end, :) = flip(par);
ovrPar = rem(ones(1, size(code,1))*code, 2);
code(1, :) = ovrPar;

codeStd = zeros(2^Np, size(data, 2));
codeStd(2:end, :) = stdForm(Np, data, par);
codeStd(1, :) = ovrPar;

function codeStd = stdForm(Np, data, parity)
% generate data + parity in standard form (exclude overall parity)

codeStd = zeros(2^Np - 1, size(data, 2));
parPos = 2.^[0:Np-1]; % parity index position
iCode = 1;
iData = 1;
iPar = 1;
while iData <= size(data, 1)
   if isempty(find(parPos == iCode, 1)) 
      % current index is for data
      codeStd(iCode, :) = data(iData, :);
      iData = iData + 1;
   else 
      % current index is for parity
      codeStd(iCode, :) = parity(iPar, :);
      iPar = iPar + 1;
   end
   iCode = iCode + 1;
end
