function [sig, err] = chkHam(codeWidth, dataWidth, code)
% generate check signature for (codeWidth, dataWidth) hamming code
% if signature is non-zero & code has 1-bit error, then LSB = 1 to indicate
% single-bit error, sig[MSB:1] indicate error bit index (0-based)
% for code with 2-bit error, LSB = 0, but error cannot be corrected
% for code with > 2-bit error, signature may not be zero, and could instead
% indicate 1-bit or 2-bit error
% IMPORTANT: code format must correspond to output from genHam()

err = false; 
Np = codeWidth - 1 - dataWidth; % # of individual parity bit excluding overall parity
if 2^Np < codeWidth
   display('ERROR: not enough code bit for given data');
   err = true;
   return;
end

Np = codeWidth - dataWidth - 1; % # of parity bit excluding overall parity
H = ones(Np + 1, 2^Np);
H(2:end, :) = binPerm(Np);
sig = rem(H*code, 2);
